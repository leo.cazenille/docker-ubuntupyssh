FROM ubuntu:18.04
MAINTAINER leo.cazenille@gmail.com

# Update system
RUN \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get upgrade -y && \
    echo "deb http://ppa.launchpad.net/ubuntu-toolchain-r/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/toolchain.list && \
    apt-get install -yq python python-pip python-dev python-yaml python3.7 python3-pip python3.7-dev python3-yaml git gosu openssh-server openssh-client rsync && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1 && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2 && \
    rm -rf /var/lib/apt/lists/* && \
    rm -fr /etc/apt/sources.list.d/toolchain.list


# Install python dependencies
RUN pip3 --no-cache-dir install numpy qdpy pandas scoop

RUN pip --no-cache-dir install numpy pandas scoop

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
